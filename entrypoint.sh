#!/bin/bash
set -e

if [[ -z "${OS_PASSWORD}" && -n "${OS_PASSWORD_FILE}" ]]; then
  export OS_PASSWORD=$(cat "${OS_PASSWORD_FILE}")
fi

exec "$@"
