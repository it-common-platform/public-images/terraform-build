FROM gitlab/gitlab-runner:ubuntu

# Unlock root account
RUN sed -i -e "/^root/s/:!:/:*:/" /etc/shadow

RUN apt-get update \
    && apt-get install -y \
       unzip \
       jq \
       git \
       curl \
       wget \
       git-lfs \
       python3-pip \
     && apt-get clean

# ------------------
# Install tfswitch.
# ------------------
ARG TERRAFORM_VERSION=1.3.6

RUN wget https://releases.hashicorp.com/terraform/1.3.6/terraform_1.3.6_linux_amd64.zip && \
    unzip -q terraform_1.3.6_linux_amd64.zip && \
    rm terraform_1.3.6_linux_amd64.zip &&\
    mv terraform /bin/

# ------------------
# Install kubectl.
# ------------------

RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/bin/kubectl && \
    kubectl version --client
